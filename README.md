# README
## Introduction
This README describes the scripts that create a database called SuperheroesDb, set up tables, create relationships between tables and add data to tables. The theme is superheroes. Run the scripts in PgAdmin and run all scripts in SuperheroesDb database.

## Scripts
- 01_tableCreate.sql: Creates Superhero, Assistant, and Power tables and sets primary keys.
- 02_relationshipSuperheroAssistant.sql: Alters Superhero and Assistant tables to add a foreign key and establishes a constraint to configure the relationship.
- 03_relationshipSuperheroPower.sql: Creates a linking table and alters Superhero and Power tables to add foreign key constraints between the linking table and the Superhero and Power tables.
- 04_insertSuperheroes.sql: Inserts three new superheroes.
- 05_insertAssistants.sql: Inserts three assistants and associates them with specific superheroes.
- 06_powers.sql: Inserts four powers and associates them with specific superheroes to demonstrate the many-to-many relationship.
- 07_updateSuperhero.sql: Updates the name of a specific superhero.
- 08_deleteAssistant.sql: Deletes a specific assistant by name.